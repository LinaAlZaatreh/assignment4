using System.Drawing;
using System.Drawing.Imaging;

namespace Assignment4;

abstract class ImageConverter: IDisposable
{
    private Bitmap _bitmap;
    private bool _disposed ;

    public ImageConverter(Bitmap bitmap)
    {
        this._bitmap = bitmap;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _bitmap.Dispose();
            }

            _disposed = true;
        }
    }

    ~ImageConverter()
    {
        Dispose(false);
    }

    static void Main(string[] args)
    {
        string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
        string imagePath = Path.Combine(directory, "Neighborhood_watch_bw.png");
        
        Bitmap originalImage = new Bitmap(imagePath);
        
        Bitmap image = CheckIfGrayscale(originalImage);
        Bitmap convertedImage = ConvertImage(image);

        convertedImage.Save("Converted.bmp", ImageFormat.Bmp);

        Console.WriteLine("Image converted successfully.");
        
        originalImage.Dispose();
        image.Dispose();
        convertedImage.Dispose();
    }
    
    static Bitmap CheckIfGrayscale(Bitmap originalImage)
    {
        if (originalImage.PixelFormat == PixelFormat.Format8bppIndexed)
        {
            Console.WriteLine("The input image is already 8-bit grayscale.");
            return null;
        }

        if (originalImage.PixelFormat != PixelFormat.Format1bppIndexed)
        {
            Console.WriteLine("The input image is not 1 bpp.");
            return null;
        }
        
        return originalImage;
    }
    
    static Bitmap ConvertImage(Bitmap image)
    {
        int width = image.Width;
        int height = image.Height;
        Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

        ColorPalette palette = convertedImage.Palette;
        for (int i = 0; i < 256; i++)
        {
            palette.Entries[i] = Color.FromArgb(i, i, i);
        }

        convertedImage.Palette = palette;

        BitmapData convertedImageData = convertedImage.LockBits(new Rectangle(0, 0, width, height),
            ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

        BitmapData originalImageData = image.LockBits(new Rectangle(0, 0, width, height),
            ImageLockMode.ReadOnly, PixelFormat.Format1bppIndexed);

        unsafe
        {
            byte* originalPointer = (byte*)originalImageData.Scan0;
            byte* convertedPointer = (byte*)convertedImageData.Scan0;

            int originalStride = originalImageData.Stride;
            int convertedStride = convertedImageData.Stride;

            for (int y = 0; y < height; y++)
            {
                byte* originalRow = originalPointer + y * originalStride;
                byte* convertedRow = convertedPointer + y * convertedStride;

                for (int i = 0; i < width / 8; i++)
                {
                    for (int b = (1<<7); b > 0; b >>= 1)
                    {
                        *convertedRow++ = (byte)((originalRow[i] & b) * 255);
                    }
                }
            }
        }
        image.UnlockBits(originalImageData);
        convertedImage.UnlockBits(convertedImageData);
        image.Dispose();

        return convertedImage;
    }
}
